package datos.manejos;

import java.sql.*;
import java.util.*;

import domain.manejos.Usuario;

public class UsuariosJDBC {
	
	//Nos apoyamos de la llave primaria autoincrementable de mysql
	//por lo que se omite el campo de usuario_id
	//se utiliza un prepareStatement, por lo que podemos utilizar parametros(signos de ?)
	//los cuales posteriormente ser�n sustituidos por el par�metro respectivo
	
	private final String SQL_INSERT = "INSERT INTO usuario(usuario, password) VALUES(?,?)";
	private final String SQL_UPDATE = "UPDATE usuario SET usuario =?, password=? WHERE id_usuario=?";
	private final String SQL_DELETE = "DELETE from usuario WHERE id_usuario=?";
	private final String SQL_SELECT = "SELECT * FROM usuario ORDER BY id_usuario";
	

	public int insert(String usuario, String password) {
		
		Connection conn = null;
		PreparedStatement stmt = null;
		// ResultSet rs = null; // no se usa en este ejercicio
		
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_INSERT);
			int index = 1;//contador de columnas
			stmt.setString(index++, usuario);//parametros 1 ?
			stmt.setString(index++, password);//parametros 2 ?
			System.out.println("Ejecutando query:" + SQL_INSERT);
			rows = stmt.executeUpdate();// no. registros afectados
			System.out.println("Registros afectados:" + rows);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			Conexion.close(conn);
			Conexion.close(stmt);
		}
		return rows;
		
	}
	
	public int update(int id_usuario, String usuario, String password) {
		
		Connection conn = null;
		PreparedStatement stmt = null;
		//ResultSet rs = null; // no se usa en este ejercicio
		
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_UPDATE);
			System.out.println("Ejecutando query:" + SQL_UPDATE);
			int index = 1;//contador de columnas
			stmt.setString(index++, usuario);//parametros 1 ?
			stmt.setString(index++, password);//parametros 2 ?
			stmt.setInt(index, id_usuario);
			rows = stmt.executeUpdate();// no. registros afectados
			System.out.println("Registros actualizados:" + rows);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			Conexion.close(conn);
			Conexion.close(stmt);
		}
		return rows;
		
	}
	
	public int delete(int id_usuario) {
		
		Connection conn = null;
		PreparedStatement stmt = null;
		//ResultSet rs = null; // no se usa en este ejercicio
		
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_DELETE);
			System.out.println("Ejecutando query:" + SQL_DELETE);
			stmt.setInt(1, id_usuario);
			rows = stmt.executeUpdate();// no. registros afectados
			System.out.println("Registros eliminados:" + rows);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			Conexion.close(conn);
			Conexion.close(stmt);
		}
		return rows;
		
	}
	
	public List<Usuario> select() {
		 
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null; 
		Usuario usuario = null;
		List<Usuario> usuarios = new ArrayList<Usuario>();
		
		try {
			conn = Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_SELECT);
			System.out.println("Ejecutando query:" + SQL_SELECT);
			rs = stmt.executeQuery();
			while(rs.next()) {
				int id_usuario = rs.getInt(1);
				String usuario1 = rs.getString(2);
				String password = rs.getString(3);
				usuario = new Usuario();
				usuario.setId_usuario(id_usuario);
				usuario.setUsuario(usuario1);
				usuario.setPassword(password);
				usuarios.add(usuario);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			Conexion.close(conn);
			Conexion.close(stmt);
			Conexion.close(rs);
		}
		return usuarios;
		
	}

	
}
