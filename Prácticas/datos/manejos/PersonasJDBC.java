package datos.manejos;
import domain.manejos.*;
import java.sql.*;
import java.util.*;

import datos.Conexion;

public class PersonasJDBC {
	
	//Nos apoyamos de la llave primaria autoincrementable de mysql
	//por lo que se omite el campo de persona_id
	//se utiliza un prepareStatement, por lo que podemos utilizar parametros(signos de ?)
	//los cuales posteriormente ser�n sustituidos por el par�metro respectivo
	
	private final String SQL_INSERT = "INSERT INTO persona(nombre, apellido) VALUES(?,?)";
	private final String SQL_UPDATE = "UPDATE persona SET nombre =?, apellido=? WHERE id_persona=?";
	private final String SQL_DELETE = "DELETE from persona WHERE id_persona=?";
	private final String SQL_SELECT = "SELECT * FROM persona ORDER BY id_persona";
	

	public int insert(String nombre, String apellido) {
		
		Connection conn = null;
		PreparedStatement stmt = null;
		// ResultSet rs = null; // no se usa en este ejercicio
		
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_INSERT);
			int index = 1;//contador de columnas
			stmt.setString(index++, nombre);//parametros 1 ?
			stmt.setString(index++, apellido);//parametros 2 ?
			System.out.println("Ejecutando query:" + SQL_INSERT);
			rows = stmt.executeUpdate();// no. registros afectados
			System.out.println("Registros afectados:" + rows);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			Conexion.close(conn);
			Conexion.close(stmt);
		}
		return rows;
		
	}
	
	public int update(int id_persona, String nombre, String apellido) {
		
		Connection conn = null;
		PreparedStatement stmt = null;
		//ResultSet rs = null; // no se usa en este ejercicio
		
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_UPDATE);
			System.out.println("Ejecutando query:" + SQL_UPDATE);
			int index = 1;//contador de columnas
			stmt.setString(index++, nombre);//parametros 1 ?
			stmt.setString(index++, apellido);//parametros 2 ?
			stmt.setInt(index, id_persona);
			rows = stmt.executeUpdate();// no. registros afectados
			System.out.println("Registros actualizados:" + rows);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			Conexion.close(conn);
			Conexion.close(stmt);
		}
		return rows;
		
	}
	
	public int delete(int id_persona) {
		
		Connection conn = null;
		PreparedStatement stmt = null;
		//ResultSet rs = null; // no se usa en este ejercicio
		
		int rows = 0;
		
		try {
			conn = Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_DELETE);
			System.out.println("Ejecutando query:" + SQL_DELETE);
			stmt.setInt(1, id_persona);
			rows = stmt.executeUpdate();// no. registros afectados
			System.out.println("Registros eliminados:" + rows);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			Conexion.close(conn);
			Conexion.close(stmt);
		}
		return rows;
		
	}
	
	public List<Persona> select() {
		 
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null; // no se usa en este ejercicio
		Persona persona = null;
		List<Persona> personas = new ArrayList<Persona>();
		
		try {
			conn = Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_SELECT);
			System.out.println("Ejecutando query:" + SQL_SELECT);
			rs = stmt.executeQuery();
			while(rs.next()) {
				int id_persona = rs.getInt(1);
				String nombre = rs.getString(2);
				String apellido = rs.getString(3);
				persona = new Persona();
				persona.setId_persona(id_persona);
				persona.setNombre(nombre);
				persona.setApellido(apellido);
				personas.add(persona);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			Conexion.close(conn);
			Conexion.close(stmt);
			Conexion.close(rs);
		}
		return personas;
		
	}

	
}
