package datos.manejos;

import java.sql.*;

public class Conexion {

	private static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static String JDBC_URL = "jdbc:mysql://localhost:3306/sga?useSSL=false";
	private static String JDBC_USER = "root";
	private static String JDBC_PASSWORD = "";
	private static Driver driver = null;
	
	public static synchronized Connection getConnection()
			throws SQLException {
		if (driver == null) {
			try {
				Class<?> jdbcDriverClass = Class.forName(JDBC_DRIVER);
				driver = (Driver) jdbcDriverClass.newInstance();
				DriverManager.registerDriver(driver);
			}catch (Exception e) {
				System.out.println("Fallo en cargar el driver JDBC");
			}
		}
		return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
	}
	
	public static void close(ResultSet rs) {
		
		try {
			if(rs != null) {
				rs.close();
			}
		}catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
	
	public static void close(PreparedStatement ps) {
		
		try {
			if(ps != null) {
				ps.close();
			}
		}catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
	
	public static void close(Connection co) {
		
		try {
			if(co != null) {
				co.close();
			}
		}catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
	
}
