package manejoArchivosCompleto;

import static utileria.Archivos.*;

public class ManejoArchivosCompleto {

	//Nota ya de estar creada la carpeta sobre la que se va a trabajar
	//y en caso necesario se deben asignar permisos de escritura en la carpeta
	
	private static final String NOMBRE_ARCHIVO = "C:\\Users\\Chema\\eclipse-java-workspace\\archivos\\documento.txt";
	
	public static void main(String args[]) {
		
		crearArchivo(NOMBRE_ARCHIVO);
		
		escribirArchivo(NOMBRE_ARCHIVO);
		
		leerArchivo(NOMBRE_ARCHIVO);
		
		anexarArchivo(NOMBRE_ARCHIVO);
		
		leerArchivo(NOMBRE_ARCHIVO);
	}
}
