package entradadatos;

import java.io.*;

public class EntradasDatos {

	public static void main (String args[]) {
		
		String captura;
		InputStreamReader input = new InputStreamReader(System.in);
		BufferedReader bInput = new BufferedReader(input);
		try {
			System.out.println("introduce un dato:");
			captura = bInput.readLine();
			while ((captura != null) || (captura == "")) {
				System.out.println("dato introducido: "  + captura);
				captura = bInput.readLine();
			}
		}catch (IOException e){
			e.printStackTrace();
		}
	}
}
