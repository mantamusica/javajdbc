package domain.excepciones;

public class OperationException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public OperationException(String mensaje) {
		
		super(mensaje);
	}

}
