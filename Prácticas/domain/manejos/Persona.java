package domain.manejos;

public class Persona {
	
	private int id_persona;
	private String nombre;
	private String apellido;
	/**
	 * @return the id_persona
	 */
	public int getId_persona() {
		return id_persona;
	}
	/**
	 * @param id_persona the id_persona to set
	 */
	public void setId_persona(int id_persona) {
		this.id_persona = id_persona;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}
	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Persona [id_persona=" + id_persona + ", nombre=" + nombre + ", apellido=" + apellido + "]";
	}
	
	

}
