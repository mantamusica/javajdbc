package usuarios.test;

import java.sql.*;
import java.util.*;

import usuarios.jdbc.*;
import usuarios.dto.*;

public class TestUsuarios {

	public static void main(String[] args) {
		
		// Utilizamos el tipo interface como referencia a una clase concreta
		UsuarioDao usuarioDao = new UsuarioDaoJdbc();
		
		/** Creamos un nuevo registro, hacemos uso de la clase usuario DTO la cual
		 * se usa para transferir la información entre las capas, no es necesario
		 * especificar la llave primaria ya que en este caso es de tipo autonumérico
		 * y la BD se encarga de asignarle un nuevo valor
		 */
		
		UsuarioDTO usuario = new UsuarioDTO();
		usuario.setUsuario("Paquete");
		usuario.setPassword("gfj765");
		//Utilizamos la capa DAO para persistir el objeto DTO
		
		try {
			//Seleccionamos todos los registros
			List<UsuarioDTO> usuarios00 = usuarioDao.select();
			for (UsuarioDTO usuarioDTO : usuarios00) {
				System.out.println(usuarioDTO);;
				System.out.println();
			}
			System.out.println("-------------------------------");
			usuarioDao.insert(usuario);
			
			//eliminamos un registro
			usuarioDao.delete(usuario);
			
			//actualizamos un registro
			UsuarioDTO usuarioTmp = new UsuarioDTO();
			usuarioTmp.setId_usuario(6);
			usuarioTmp.setUsuario("Luis");
			usuarioTmp.setPassword("sgdf7645ghf");
			usuarioDao.update(usuarioTmp);
			
			//Seleccionamos todos los registros
			List<UsuarioDTO> usuarios01 = usuarioDao.select();
			for (UsuarioDTO usuarioDTO : usuarios01) {
				System.out.println(usuarioDTO);
				System.out.println();
			}
			System.out.println("-------------------------------");
		}catch (SQLException e) {
			System.out.println("Excepcion en la capa de prueba");
			e.printStackTrace();
		}
		
	}

}
