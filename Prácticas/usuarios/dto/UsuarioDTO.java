package usuarios.dto;

public class UsuarioDTO {
	
	private int id_usuario;
	private String usuario;
	private String password;
	

	public UsuarioDTO() {
	}
	
	
	public UsuarioDTO(int id_usuario) {
		this.id_usuario = id_usuario;

	}

	

	/**
	 * @return the id_usuario
	 */
	public int getId_usuario() {
		return id_usuario;
	}


	/**
	 * @param id_usuario the id_usuario to set
	 */
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}


	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}


	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UsuarioDTO [id_usuario=" + id_usuario + ", usuario=" + usuario + ", password=" + password + "]";
	}

	
}
