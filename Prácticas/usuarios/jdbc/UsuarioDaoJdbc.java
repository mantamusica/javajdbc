package usuarios.jdbc;

import java.sql.*;
import java.util.*;

import usuarios.dto.*;

/** Esta clase implemente la clase PersonaDAO, es una implementación con la 
 * tecnología JDBC podría haber otro tipo de implementaciones con tecnologías
 * como Hibernate, iBatis, SpringJDBC ...
 * @author Chema
 *
 */

public class UsuarioDaoJdbc implements UsuarioDao{
	
	private Connection userConn;
	
	private final String SQL_INSERT = "INSERT INTO usuario(usuario, password) VALUES(?,?)";
	private final String SQL_UPDATE = "UPDATE usuario SET usuario =?, password=? WHERE id_usuario=?";
	private final String SQL_DELETE = "DELETE from usuario WHERE id_usuario=?";
	private final String SQL_SELECT = "SELECT * FROM usuario ORDER BY id_usuario";
	
	public UsuarioDaoJdbc() {
		
	}	

	public UsuarioDaoJdbc(Connection userConn) {
		this.userConn = userConn;
	}

	@Override
	public int insert(UsuarioDTO usuario) throws SQLException {

		Connection conn = null;
		PreparedStatement stmt = null;		
		int rows = 0;
		
		try {
			conn = (this.userConn != null) ? this.userConn : Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_INSERT);
			int index = 1;//contador de columnas
			stmt.setString(index++, usuario.getUsuario());//parametros 1 ?
			stmt.setString(index++, usuario.getPassword());//parametros 2 ?
			System.out.println("Ejecutando query: " + SQL_INSERT);
			rows = stmt.executeUpdate();// no. registros afectados
			System.out.println("Registros afectados: " + rows);
		}finally {
			Conexion.close(stmt);
			if(this.userConn == null) {
				Conexion.close(conn);
			}
		}
		return rows;
	}

	@Override
	public int update(UsuarioDTO usuario) throws SQLException {

		Connection conn = null;
		PreparedStatement stmt = null;		
		int rows = 0;
		
		try {
			conn = (this.userConn != null) ? this.userConn : Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_UPDATE);
			int index = 1;//contador de columnas
			stmt.setString(index++, usuario.getUsuario());//parametros 1 ?
			stmt.setString(index++, usuario.getPassword());//parametros 2 ?
			stmt.setInt(index, usuario.getId_usuario());
			System.out.println("Ejecutando query: " + SQL_INSERT);
			rows = stmt.executeUpdate();// no. registros afectados
			System.out.println("Registros afectados: " + rows);
		}finally {
			Conexion.close(stmt);
			if(this.userConn == null) {
				Conexion.close(conn);
			}
		}
		return rows;
	}

	@Override
	public int delete(UsuarioDTO usuario) throws SQLException {

		Connection conn = null;
		PreparedStatement stmt = null;		
		int rows = 0;
		
		try {
			conn = (this.userConn != null) ? this.userConn : Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_DELETE);
			stmt.setInt(1, usuario.getId_usuario());
			System.out.println("Ejecutando query: " + SQL_INSERT);
			rows = stmt.executeUpdate();// no. registros afectados
			System.out.println("Registros afectados: " + rows);
		}finally {
			Conexion.close(stmt);
			if(this.userConn == null) {
				Conexion.close(conn);
			}
		}
		return rows;
	}

	@Override
	public List<UsuarioDTO> select() throws SQLException {

		Connection conn = null;
		PreparedStatement stmt = null;	
		ResultSet rs = null;
		UsuarioDTO  usuarioDTO = null;
		List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
		
		try {
			conn = (this.userConn != null) ? this.userConn : Conexion.getConnection();
			stmt = conn.prepareStatement(SQL_SELECT);
			rs = stmt.executeQuery();
			while(rs.next()) {
				int idUsuarioTemp = rs.getInt(1);
				String usuarioTemp = rs.getString(2);
				String passwordTemp = rs.getString(3);
				usuarioDTO = new UsuarioDTO();
				usuarioDTO.setId_usuario(idUsuarioTemp);
				usuarioDTO.setUsuario(usuarioTemp);
				usuarioDTO.setPassword(passwordTemp);
				usuarios.add(usuarioDTO);
			}
		}finally {
			Conexion.close(rs);
			Conexion.close(stmt);
			if(this.userConn == null) {
				Conexion.close(conn);
			}
		}
		return usuarios;
	}

}
