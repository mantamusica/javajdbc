package usuarios.jdbc;

import java.sql.*;
import java.util.*;

import usuarios.dto.*;

/**Esta interfaz contiene los m�todos abstractos con las operaciones b�sicas sobre la 
 * tabla de Persona CRUD, se debe crear una clase concreta para implementar el c�digo
 * asociado a cada m�todo
 */

public interface UsuarioDao {


	public int insert (UsuarioDTO usuario) throws SQLException;
	public int update (UsuarioDTO usuario) throws SQLException;
	public int delete (UsuarioDTO usuario) throws SQLException;
	public List<UsuarioDTO> select () throws SQLException;
}
