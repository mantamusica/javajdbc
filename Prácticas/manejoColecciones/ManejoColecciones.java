package manejoColecciones;

import java.util.*;

public class ManejoColecciones {

	
	public static void main(String args[]) {
	
		List<Object> miLista = new ArrayList<Object>();
		miLista.add("1");
		miLista.add("2");
		miLista.add("3");
		miLista.add("4");	
		//Elemento repetido
		miLista.add("4");
		imprimir(miLista);
		
		Set<Object> miSet = new HashSet<Object>();
		miSet.add("100");
		miSet.add("200");
		miSet.add("1000");
		//NO permite elementos repeitidos, los ignora
		miSet.add("1000");
		imprimir(miSet);
		
		Map<Comparable<String>, Comparable<String>> miMapa = new HashMap<Comparable<String>, Comparable<String>>();
		miMapa.put("1", "Juan");
		miMapa.put("2", "John");
		miMapa.put("3", "Luis");
		miMapa.put("4", "Juan");
		//se imprimen todas las llaves
		imprimir(miMapa.keySet());
		//se imprimen todos los valores
		imprimir(miMapa.values());
		
		Vector<Object> miVector = new Vector<Object>();
		miVector.add(1);
		miVector.add(2);
		miVector.add(3);
		//se imprimen todos los vectores
		imprimir(miVector);
		
	
	}

	private static void imprimir(Collection<?> coleccion) {

		for(Object elemento : coleccion) {
			System.out.print(elemento + " ");
			
		}
		
		System.out.println(" ");
	}
}
