package manejoColeccionesGenericas;

import java.util.*;

public class ManejoColeccionesGenericas {

	public static void main(String[] args) {

		List<String> miLista = new ArrayList<>();
		miLista.add("1");
		miLista.add("2");
		miLista.add("3");
		miLista.add("4");
		miLista.add("4");
		
		imprimir(miLista);
		
		Set<String> miSet = new HashSet<>();
		miSet.add("100");
		miSet.add("200");
		miSet.add("300");
		miSet.add("400");
		miSet.add("400");
		
		imprimir(miSet);
		
		Map<String, String> miMapa = new HashMap<>();
		
		miMapa.put("1", "Juan");
		miMapa.put("2", "Luis");
		miMapa.put("3", "Rocio");
		miMapa.put("4", "Manu");
		
		imprimir(miMapa.keySet());
		imprimir(miMapa.values());
		

	}
	
	static void imprimir(Collection<String> coleccion) {
		
		for (String elemento : coleccion) {
			
			System.out.print(elemento +  " ");
		}
		System.out.println(" ");
	}

}
