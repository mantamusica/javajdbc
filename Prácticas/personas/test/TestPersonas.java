package personas.test;

import java.sql.*;
import java.util.*;
import personas.dto.*;
import personas.jdbc.*;

public class TestPersonas {

	public static void main(String[] args) {
		
		// Utilizamos el tipo interface como referencia a una clase concreta
		PersonaDao personaDao = new PersonaDaoJdbc();
		
		/** Creamos un nuevo registro, hacemos uso de la clase persona DTO la cual
		 * se usa para transferir la información entre las capas, no es necesario
		 * especificar la llave primaria ya que en este caso es de tipo autonumérico
		 * y la BD se encarga de asignarle un nuevo valor
		 */
		
		PersonaDTO persona = new PersonaDTO();
		persona.setNombre("Fernando");
		persona.setApellido("López");
		//Utilizamos la capa DAO para persistir el objeto DTO
		
		try {
			//Seleccionamos todos los registros
			List<PersonaDTO> personas00 = personaDao.select();
			for (PersonaDTO personaDTO : personas00) {
				System.out.println(personaDTO);;
				System.out.println();
			}
			System.out.println("-------------------------------");
			personaDao.insert(persona);
			
			//eliminamos un registro
			personaDao.delete(persona);
			
			//actualizamos un registro
			PersonaDTO personaTmp = new PersonaDTO();
			personaTmp.setId_persona(6);
			personaTmp.setNombre("Luis");
			personaTmp.setApellido("Manejo");
			personaDao.update(personaTmp);
			
			//Seleccionamos todos los registros
			List<PersonaDTO> personas01 = personaDao.select();
			for (PersonaDTO personaDTO : personas01) {
				System.out.println(personaDTO);
				System.out.println();
			}
			System.out.println("-------------------------------");
		}catch (SQLException e) {
			System.out.println("Excepcion en la capa de prueba");
			e.printStackTrace();
		}
		
	}

}
