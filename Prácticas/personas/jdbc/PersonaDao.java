package personas.jdbc;

import java.sql.*;
import java.util.*;
import personas.dto.*;

/**Esta interfaz contiene los m�todos abstractos con las operaciones b�sicas sobre la 
 * tabla de Persona CRUD, se debe crear una clase concreta para implementar el c�digo
 * asociado a cada m�todo
 */

public interface PersonaDao {


	public int insert (PersonaDTO persona) throws SQLException;
	public int update (PersonaDTO persona) throws SQLException;
	public int delete (PersonaDTO persona) throws SQLException;
	public List<PersonaDTO> select () throws SQLException;
}
