/*
 * Derechos reservados GlobalMentoring.com.mx
 */
package ejemploJavaDoc;


/**
 * Clase para probar el concepto de JavaDoc
 * @version 1.0
 */
public class EjemploJavaDoc {

    /**
     * Metodo que ejecuta la prueba de la clase Aritmetica
     * @param args argumentos de la linea de comandos
     */
    public static void main(String[] args) {
        int resultado = (3+2);

        System.out.println("resultado:" + resultado);
    }
}