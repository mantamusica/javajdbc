package manejoexcepciones2;
import datos.excepciones.*;
import excepciones.*;

public class ManejoExcepciones2 {

	public static void main(String[] args) throws Excepcion {

		AccesoDatos datos = new ImplementacionMySql();
		datos.simularError(true);
		ejecutar(datos, "listar");
		
		datos.simularError(false);
		System.out.println("");
		ejecutar(datos, "insertar");
	}

	private static void ejecutar(AccesoDatos datos, String action) throws Excepcion {
		
		if("listar".equals(action)) {
			try {
				datos.listar();
			}
			catch (LecturaDatosEx ex) {
				System.out.println("Error lectura: Procesa la excepcion mas especifica de lectura de datos");
			}
			catch (AccesoDatosEx ex) {
				System.out.println("Error acceso datos: Procesa la excepcion mas especifica de acceso de datos");
			}
			finally {
				System.out.println("Procesar finally es opcional, siempre se ejecutar� sin importar si hubo error o no.");
			}
		}else if("insertar".equals(action)) {
			try {
				datos.insertar();
			}
			catch (AccesoDatosEx ex) {
				System.out.println("Error acceso datos: Procesa la excepcion mas especifica de acceso de datos");
			}
			finally {
				System.out.println("Procesar finally es opcional, siempre se ejecutar� sin importar si hubo error o no.");
			}
		}else {
			System.out.println("No se proporcion� ninguna acci�n conocida.");
		}
	}
}
