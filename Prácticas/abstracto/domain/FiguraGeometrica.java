package abstracto.domain;

public abstract class FiguraGeometrica {

	protected String tipoFigura;
	
	protected FiguraGeometrica(String tipoFigura) {
		this.tipoFigura = tipoFigura;
	}
	
	public abstract void dibujar();

	/**
	 * @return the tipoFigura
	 */
	public String getTipoFigura() {
		return tipoFigura;
	}

	/**
	 * @param tipoFigura the tipoFigura to set
	 */
	public void setTipoFigura(String tipoFigura) {
		this.tipoFigura = tipoFigura;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FiguraGeometrica [tipoFigura=" + tipoFigura + "]";
	}
	
	

}
