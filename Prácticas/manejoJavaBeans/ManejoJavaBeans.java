package manejoJavaBeans;

import beans.PersonaBean;

public class ManejoJavaBeans {

	public static void main(String[] args){
		
		PersonaBean bean = new PersonaBean();
		bean.setNombre("juan");
		bean.setEdad(34);
		
		System.out.println("Nombre:" + bean.getNombre());
		System.out.println("Edad:" + bean.getEdad());
	}
}
