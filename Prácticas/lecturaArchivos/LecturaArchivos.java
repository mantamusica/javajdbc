package lecturaArchivos;

import java.io.*;

public class LecturaArchivos {

	public static void main(String[] args) {

		File archivo = new File("C:\\Users\\Chema\\eclipse-java-workspace\\archivos\\documento.txt");
		
		try {
			
			BufferedReader entrada = new BufferedReader(new FileReader(archivo));
			String lectura;
			lectura = entrada.readLine();
			while (lectura != null){
				System.out.println("Lectura: " + lectura);
				lectura = entrada.readLine();
			}
			entrada.close();
		}catch(FileNotFoundException fnf) {
			fnf.printStackTrace();
		}catch(IOException ex) {
			ex.printStackTrace();
		}

	}

}
