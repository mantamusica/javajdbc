package manejoGenericos;

public class ManejoGenericos {
	
	public static void main(String args[] ){
		
		//Creamos una instancia de clase generica para integer
		ClaseGenerica<Integer> objetoInt = new ClaseGenerica<Integer>(15);
		objetoInt.obtenerTipo();
		
		//Creamos una instancia de clase generica para string
		ClaseGenerica<String> objetoString = new ClaseGenerica<String>("Casa");
		objetoString.obtenerTipo();
		
		//Los genericos no pueden ser aplicados a tipos primitivos
		//POr lo que esto marcaria un error de compilacion
		//ClaseGenerica<Int> objetoInt = new ClaseGenerica<Int>(15);
		
	}

}
