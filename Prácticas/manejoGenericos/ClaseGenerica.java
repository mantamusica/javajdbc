package manejoGenericos;

//Definimos una clase generica con el operador diamente<>
public class ClaseGenerica<T> {

	//Definimos una variable de tipo generico
	T objeto;
	
	//Constructor que inicialia el tipo a utilizar
	public ClaseGenerica(T objeto) {
		this.objeto = objeto;
	}
	
	public void obtenerTipo() {
		
		System.out.println("El tipo T es: " + objeto.getClass().getName());
	}

	/**
	 * @return the objeto
	 */
	public T getObjeto() {
		return objeto;
	}

	/**
	 * @param objeto the objeto to set
	 */
	public void setObjeto(T objeto) {
		this.objeto = objeto;
	}
	
	
	
	
	
}
