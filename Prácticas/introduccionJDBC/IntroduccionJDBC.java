package introduccionJDBC;

import java.sql.*;

public class IntroduccionJDBC {

	public static void main(String[] args) {

		//Cadena de conexion de mysql, el parametro useSSl es opcional
		String url = "jdbc:mysql://localhost:3306/sga?useSSL=false";
		//Cargamos el driver de mysql
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Creamos el objeto de conexion
			Connection conexion = (Connection) DriverManager.getConnection(url, "root", "");
			//Creamos un objeto statement
			Statement instruccion = conexion.createStatement();
			//Creamos el query
			String sql = "select id_persona, nombre, apellido from persona";
			ResultSet result = instruccion.executeQuery(sql);
			while(result.next()) {
				System.out.println("id: " + result.getInt(1)) ;
				System.out.println("nombre: " + result.getString(2)) ;
				System.out.println("apellido: " + result.getString(3)) ;
			}
			//Cerramos objetos usados
			result.close();
			instruccion.close();
			conexion.close();
		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

}


