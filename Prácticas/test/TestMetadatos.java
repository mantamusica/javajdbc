package test;

import datos.Conexion;
import java.sql.*;
import oracle.jdbc.*;

public class TestMetadatos {

	public static void main(String[] args) {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con = Conexion.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery("SELECT * FROM employees");
			//Obtenemos un objeto de metadatos de Oracle
			OracleResultSetMetaData rsOracle = (OracleResultSetMetaData) rs.getMetaData();
			
			if(rsOracle == null) {
				System.out.println("Metadatos no disponibles");
			}else {
				//Preguntamos cuantas columnas tiene tabla de empleados
				int columnCount = rsOracle.getColumnCount();
				
				//Desplegamos el numero de columnas
				System.out.println("N�mero de columnas:" + columnCount);
				
				for (int i = 1; i<= columnCount; i++) {
					//Desplegamos el nombre de la columna
					System.out.println("Nombre de columna:" + rsOracle.getColumnName(i));
					//Desplegamos el tipo de la columna
					System.out.println("Tipo de columna:" + rsOracle.getColumnTypeName(i));
					//Desplegamos si la columna puede almacenar valores nulos
					switch (rsOracle.isNullable(i)) {
					case OracleResultSetMetaData.columnNoNulls:
					System.out.print(", No acepta nulos");
					break;
					case OracleResultSetMetaData.columnNullable:
					System.out.print(", Si acepta nulos");
					break;
					case OracleResultSetMetaData.columnNullableUnknown:
					System.out.print(", Valor nulo desconocido");
					}
					System.out.println("");
					
					
				}
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			Conexion.close(rs);
			Conexion.close(con);
		}

	}

}
