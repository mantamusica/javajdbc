package manejoArchivos;

import java.io.*;
public class ManejoArchivos {

	public static void main(String[] args) {

		File archivo = new File("C:\\Users\\Chema\\eclipse-java-workspace\\archivos\\documento.txt");
		try {
			PrintWriter salida = new PrintWriter(new FileWriter(archivo));
			salida.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
		System.out.println("El archivo se ha creado correctamente.");
	}

}
