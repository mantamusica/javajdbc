package manejoExcepciones1;

import domain.excepciones.Division;
import domain.excepciones.OperationException;

public class ManejoExcepciones1 {

	public static void main(String[] args) {
		
		try {
			
			Division division0 = new Division(10,1);
			division0.visualizarOperacion();
			
			Division division = new Division(10,0);
			division.visualizarOperacion();
			
		}catch(OperationException oe) {
			
			System.out.println("Ocurrio un error.");
			oe.printStackTrace();
			
		}
	}
}
