package calculadoraEntradaDatos;

import java.util.*;
import static calculadora.Operaciones.sumar;

public class CalculadoraEntradaDatos {
	
	private static Scanner scan1;
	private static Scanner scan2;

	public static void main (String args[] ){
		System.out.println("Proporciona el primer valor:");
		scan1 = new Scanner(System.in);
		int a = scan1.nextInt();
		
		System.out.println("Proporciona el segundo valor:");
		scan2 = new Scanner(System.in);
		int b = scan2.nextInt();
		
		int resultado = sumar(a,b);
		
		System.out.println("El resultado es : " + resultado);
		
		
		
	}

	
}
