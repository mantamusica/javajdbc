package entradadatosscanner;

import java.util.*;

public class EntradaDatosScanner {

	private static Scanner scan;

	public static void main(String[] args) {
		String captura = null;
		scan = new Scanner(System.in);
		System.out.println("introduce un dato:");
		captura = scan.nextLine();
		
		while ((captura != null) || (captura == "")) {
			
			System.out.println("dato introducido: "  + captura);
			captura = scan.nextLine();
		}
	}
}
